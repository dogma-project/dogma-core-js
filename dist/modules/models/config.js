"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Types = __importStar(require("../../types"));
const datadir_1 = require("../datadir");
const nedb_1 = __importDefault(require("@seald-io/nedb"));
const logger_1 = __importDefault(require("../logger"));
class ConfigModel {
    constructor({ state }) {
        this.db = new nedb_1.default({
            filename: datadir_1.nedbDir + "/config.db",
        });
        this.stateBridge = state;
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                logger_1.default.debug("nedb", "load database", "config");
                yield this.db.loadDatabaseAsync();
                yield this.db.ensureIndexAsync({
                    fieldName: "param",
                    unique: true,
                });
                this.stateBridge.emit("CONFIG DB" /* Types.Event.Type.configDb */, 2 /* Types.System.States.ready */);
            }
            catch (err) {
                logger_1.default.error("config.nedb", err);
            }
        });
    }
    loadConfigTable() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                logger_1.default.log("Config Model", "Load config table");
                const data = yield this.getAll();
                if (data.length) {
                    // add condition
                    data.forEach((element) => {
                        const type = element.param;
                        this.stateBridge.emit(type, element.value);
                    });
                }
                else {
                    this.stateBridge.emit("CONFIG DB" /* Types.Event.Type.configDb */, 3 /* Types.System.States.empty */);
                }
            }
            catch (err) {
                logger_1.default.error("config.nedb", err);
            }
        });
    }
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.db.findAsync({});
        });
    }
    persistConfig(config) {
        return __awaiter(this, void 0, void 0, function* () {
            const newObject = Object.keys(config).map((key) => ({
                param: key,
                value: config[key],
            }));
            for (const row of newObject)
                yield this.db.updateAsync({ param: row.param }, row, { upsert: true });
            this.stateBridge.emit("CONFIG DB" /* Types.Event.Type.configDb */, 4 /* Types.System.States.reload */); // downgrade state to reload database
        });
    }
}
exports.default = ConfigModel;
