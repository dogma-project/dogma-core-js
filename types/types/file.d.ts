declare namespace File {
    type Description = {
        descriptor: number;
        size: number;
        pathname: string;
        data?: string;
    };
}
export default File;
