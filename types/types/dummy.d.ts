import Streams from "./streams";
declare namespace Dummmy {
    type Abstract = {
        class: Streams.MX.dummy;
        body: null;
    };
}
export default Dummmy;
