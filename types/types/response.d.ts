declare namespace Response {
    type Main = {
        id: number | string;
        code: number;
        message?: string;
    };
}
export default Response;
