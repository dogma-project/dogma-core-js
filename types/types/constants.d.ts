declare namespace Constants {
    const enum Boolean {
        false = 0,
        true = 1
    }
}
export default Constants;
