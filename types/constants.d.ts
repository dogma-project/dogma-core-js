export declare const API: {
    OK: number;
    CANNOTGETCERT: number;
    INVALIDCERT: number;
    ADDCERTERROR: number;
    CREATEDBERROR: number;
    GETCONFIGERROR: number;
    CONFIGSAVEERROR: number;
    CANNOTGETMSG: number;
    CANNOTPUSHMSG: number;
    CANNOTGETFRIENDS: number;
    CANNOTCREATEMK: number;
    CANNOTCREATENK: number;
    CANNOTGETSERVICES: number;
    CANNOTGETCONNECTIONS: number;
    CANNOTDELETEFRIEND: number;
    CANNOTDELETEITSELF: number;
};
/**
 * @constant
 * @type {Object}
 * @default
 */
export declare const DEFAULTS: {
    UNKNOWN: number;
    ROUTER: number;
    LOG_LEVEL: number;
    EXTERNAL: string;
    AUTO_DEFINE_IP: number;
    USER_NAME: string;
    NODE_NAME: string;
    LOCAL_DISCOVERY_PORT: number;
};
/**
 * @constant
 * @type {Object}
 * @default
 */
export declare const PROTOCOL: {
    DB: number;
    CERTIFICATE: number;
};
export declare const MSG_FORMAT: {
    DEFAULT: number;
    FILES: number;
    ATTACHMENTS: number;
    COMMON: number;
};
export declare const MSG_CODE: {
    ERROR: number;
    UNKNOWN: number;
    SUCCESS: number;
    CONFIRMED: number;
};
export declare const DESCRIPTOR: {
    SIZE: number;
};
export declare const SIZES: {
    MX: number;
};
export declare const ATTACHMENTS: {
    FILE: number;
    IMAGE: number;
    GIF: number;
    AUDIO: number;
    VIDEO: number;
    VOICE: number;
    VIDEO_MSG: number;
};
