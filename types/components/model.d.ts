import { ConfigModel, NodeModel, DHTModel, UserModel } from "../modules/model";
declare const configModel: ConfigModel;
declare const nodeModel: NodeModel;
declare const dhtModel: DHTModel;
declare const userModel: UserModel;
export { dhtModel, nodeModel, userModel, configModel };
