declare const obj: {
    auto: boolean;
    discovery: boolean;
    master: string | undefined;
    node: string | undefined;
    port: string | undefined;
    logLevel: string | undefined;
    prefix: string | undefined;
    ifport: string | undefined;
};
export default obj;
