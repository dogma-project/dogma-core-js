export function readUsersTable(): Promise<any>;
export function readNodesTable(): Promise<any>;
export function readProtocolTable(): Promise<any>;
