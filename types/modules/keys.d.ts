import { Keys } from "../types";
export declare function createKeyPair(type: Keys.Type, length?: Keys.InitialParams["keylength"]): Promise<boolean>;
