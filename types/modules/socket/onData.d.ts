import * as Types from "../../types";
import DogmaSocket from "../socket";
export default function onData(this: DogmaSocket, result: Types.Streams.DemuxedResult): void;
