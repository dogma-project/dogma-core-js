import * as Types from "../../types";
export default function response(id: number | string, code: number, message?: string): Types.Response.Main;
