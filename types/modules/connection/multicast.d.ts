import * as Types from "../../types";
import Connections from "../connections";
export default function multicast(this: Connections, request: Types.Request, destination: Types.Connection.Group): void;
