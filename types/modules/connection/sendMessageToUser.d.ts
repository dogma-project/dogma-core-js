declare function _exports(user_id: string, message: {
    text: string;
    files: any[];
}): Promise<any>;
export = _exports;
