import * as Types from "../../types";
import Connections from "../connections";
export default function send(this: Connections, request: Types.Request, user_id: Types.User.Id): void;
