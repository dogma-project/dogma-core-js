import Connections from "../connections";
import * as Types from "../../types";
export default function closeConnecion(this: Connections, user_id: Types.User.Id): void;
