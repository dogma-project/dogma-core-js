import * as Types from "../../types";
import Connections from "../connections";
export default function send(this: Connections, request: Types.Request, node_id: Types.Node.Id): Types.Response.Main | undefined;
