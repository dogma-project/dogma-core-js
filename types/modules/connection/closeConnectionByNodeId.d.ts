import ConnectionClass from "../connections";
import * as Types from "../../types";
export default function closeConnecion(this: ConnectionClass, node_id: Types.Node.Id): void;
