declare function _exports(node_id: string, message: {
    text: string;
    files: any[];
}, type?: number): Promise<any>;
export = _exports;
