import * as Types from "../types";
/**
 * @param size *2
 */
declare const generateSyncId: (size?: number) => Types.Sync.Id;
export default generateSyncId;
