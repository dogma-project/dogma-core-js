declare const _default: {
    String: StringConstructor;
    Array: ArrayConstructor;
};
export default _default;
