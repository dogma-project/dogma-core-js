/**
 * @module GetLocalAddress
 * @param {String} family default: IPv4
 * @returns {Array} array of ip's
 */
export declare const getLocalAddress: (family?: string) => any[];
