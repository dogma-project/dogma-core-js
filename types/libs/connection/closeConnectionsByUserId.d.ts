import ConnectionClass from "../connection";
import { Types } from "../../types";
export default function closeConnecion(this: ConnectionClass, user_id: Types.User.Id): Promise<void>;
