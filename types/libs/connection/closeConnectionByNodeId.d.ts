import ConnectionClass from "../connection";
import { Types } from "../../types";
export default function closeConnecion(this: ConnectionClass, node_id: Types.Node.Id): Promise<void>;
