import { Types } from "../../types";
import ConnectionClass from "../connection";
export default function offline(this: ConnectionClass, node_id: Types.Node.Id): void;
