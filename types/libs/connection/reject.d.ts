import { Types } from "../../types";
declare const reject: (socket: Types.Connection.Socket, ...message: any) => void;
export default reject;
