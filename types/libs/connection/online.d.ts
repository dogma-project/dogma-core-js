import { Types } from "../../types";
import ConnectionClass from "../connection";
export default function online(this: ConnectionClass, node_id: Types.Node.Id): void;
