declare namespace Response {
  export type Main = {
    id: number | string;
    code: number;
    message?: string;
  };
}

export default Response;
