declare namespace Constants {
  export const enum Boolean {
    false = 0,
    true = 1,
  }
}

export default Constants;
