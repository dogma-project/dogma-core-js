import StateManager from "../modules/state";

const stateManager = new StateManager();

export default stateManager;
